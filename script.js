class BattleshipGame {
	constructor() {
		// config
		this.boardSize = 10; // size of board 10x10
		this.length = {length: this.boardSize}
		this.shipSizes = [
			4, // A one four-part ship
			3, 3, // Two three-part ships
			2, 2, 2, // Three two-part ships
			1, 1, 1, 1 // Four one-part ships
		];
		// --------------------------------------------
		// variables below will use on start simulation
		// --------------------------------------------
		this.ships = []; // All coordinates of ships
		// AI1
		this.ai1Shots = []; // shots history of AI1
		// AI2
		this.spiralPath = this.initializeSpiralPath();
		this.steps = 0;
		// stats
		this.ai1Wins = 0;
		this.ai2Wins = 0;
	}

	initializeBoard() {
		return Array.from(this.length, () =>
			Array.from(this.length, () => ({
				ship: false,
				shipSafeZone: false,
				hitA: false,
				hitB: false,
			}))
		);
	}

	getRandomPosition() {
		return {
			row: Math.floor(Math.random() * this.boardSize),
			col: Math.floor(Math.random() * this.boardSize),
		};
	}

	isValidPosition(row, col) {
		return row >= 0 && row < this.boardSize && col >= 0 && col < this.boardSize;
	}

	isCellEmpty(row, col) {
		return this.isValidPosition(row, col) && !this.board[row][col].ship;
	}

	placeShip(shipSize) {
		let shipPlaced = false;
		let ship = [];

		while (!shipPlaced) {
			const {row, col} = this.getRandomPosition();
			const horizontal = Math.random() < 0.5;

			let validPlacement = true;

			if (horizontal) {
				if (col + shipSize >= this.boardSize) {
					validPlacement = false;
				} else {
					for (let i = col; i < col + shipSize; i++) {
						if (!this.isCellEmpty(row, i) || !this.isSafeZoneEmpty(row, i) || i >= this.boardSize) {
							validPlacement = false;
							break;
						}
					}
				}
			} else {
				if (row + shipSize >= this.boardSize) {
					validPlacement = false;
				} else {
					for (let i = row; i < row + shipSize; i++) {
						if (!this.isCellEmpty(i, col) || !this.isSafeZoneEmpty(i, col) || i >= this.boardSize) {
							validPlacement = false;
							break;
						}
					}
				}
			}

			if (validPlacement) {
				if (horizontal) {
					for (let i = col; i < col + shipSize; i++) {
						this.board[row][i].ship = true;
						ship.push({row, col: i});
					}
					this.setSafeZone(ship)
				} else {
					for (let i = row; i < row + shipSize; i++) {
						this.board[i][col].ship = true;
						ship.push({row: i, col});
					}
					this.setSafeZone(ship)
				}

				shipPlaced = true;
			}
		}

		this.ships.push(ship);
	}

	setSafeZone(ship) {
		const startRow = ship.reduce((min, current) => (current.row < min ? current.row : min), ship[0].row) - 1;
		const endRow = ship.reduce((max, current) => (current.row > max ? current.row : max), ship[0].row) + 1;

		const startCol = ship.reduce((min, current) => (current.col < min ? current.col : min), ship[0].col) - 1;
		const endCol = ship.reduce((max, current) => (current.col > max ? current.col : max), ship[0].col) + 1;

		for (let r = startRow; r <= endRow; r++) {
			for (let c = startCol; c <= endCol; c++) {
				if (r < 0 || r >= this.boardSize || c < 0 || c >= this.boardSize) continue
				this.board[r][c].shipSafeZone = true;
			}
		}
	}

	isSafeZoneEmpty(row, col) {
		return !this.board[row][col].shipSafeZone
	}

	placeShips() {
		this.shipSizes.map(shipSize => this.placeShip(shipSize));
	}

	isShipHit(ship, shot) {
		return ship.some((cell) => cell.row === shot.row && cell.col === shot.col);
	}

	isGameOver() {
		return this.ships.every((ship) => ship.every((cell) => cell.hitA || cell.hitB));
	}

	ai1Shoot() {
		let shot = null;

		do {
			shot = this.getRandomPosition();
		} while (this.ai1Shots.some((s) => s.row === shot.row && s.col === shot.col));

		return shot;
	}

	ai2Shoot() {
		const cell = this.spiralPath[this.steps]
		this.steps++
		return {row: cell[0], col: cell[1]}
	}

	render() {
		const wrapper = document.querySelector(".wrapper")
		// reset
		wrapper.innerHTML = null;
		this.board.map(row => {
			let column = document.createElement("div")
			column.classList.add("wrapper")
			// console.log(row)
			const ships = row.map(s => {
				let ship = document.createElement("div")
				ship.classList.add("ship")
				if (s.shipSafeZone) {
					ship.classList.add("safezone")
				}
				if (s.hitA) {
					ship.classList.add("hitA")
				}
				if (s.hitB) {
					ship.classList.add("hitB")
				}
				if (s.ship) {
					ship.innerHTML = "🚢"
				}
				return ship
			})
			ships.map(e => {
				column.append(e)
			})
			// add new elements
			wrapper.append(column)
		})

		// console.table(this.board)
	}

	playGame = async () => {
		while (!this.isGameOver()) {
			this.render()

			console.log(await new Promise((resolve, reject) => {
				setTimeout(() => resolve("готово!"), 50)
			}))

			const ai1Shot = this.ai1Shoot();
			const ai2Shot = this.ai2Shoot();

			this.ai1Shots.push(ai1Shot);

			const ai1Hit = this.ships.some((ship) => this.isShipHit(ship, ai1Shot));
			const ai2Hit = this.ships.some((ship) => this.isShipHit(ship, ai2Shot));

			if (ai1Hit) {
				this.ships.map(ship => {
					if (this.isShipHit(ship, ai1Shot)) {
						ship.map(cell => {
							if (cell.row === ai1Shot.row && cell.col === ai1Shot.col) {
								this.board[ai1Shot.row][ai1Shot.col].hitA = true;
								cell.hitA = true;
							}
						});
					}
				});
			}

			if (ai2Hit) {
				this.ships.map(ship => {
					if (this.isShipHit(ship, ai2Shot)) {
						ship.map(cell => {
							if (cell.row === ai2Shot.row && cell.col === ai2Shot.col) {
								this.board[ai2Shot.row][ai2Shot.col].hitB = true;
								cell.hitB = true;
							}
						});
					}
				});
			}
		}

		if (this.ai1Shots.length < this.steps + 1) {
			this.ai1Wins++;
		} else {
			this.ai2Wins++;
		}
	}

	initializeSpiralPath() {
		const a = Array.from(this.length, (_, row) =>
			Array.from(this.length, (_, col) => {
					return [row, col]
				}
			))

		a.reverse();
		let b = []
		for (b = []; a.length;) b.push(...a.shift()), a.map(c => b.push(c.pop())), a.reverse().map(c => c.reverse());
		return b.reverse()
	}

	reset() {
		this.board = this.initializeBoard();
		this.ships = [];
		this.ai1Shots = [];
		this.steps = 0;
	}

	async runSimulation(numGames) {
		const buttonStart = document.querySelector(".start")
		const buttonReset = document.querySelector(".reset")
		buttonStart.addEventListener("click", this.playGame)
		buttonReset.addEventListener("click", this.reset)

		for (let i = 0; i < numGames; i++) {
			console.log(`Game ${i + 1}`);
			console.log("=======");
			this.reset()
			this.placeShips()
			await this.playGame();
			console.log(`AI1/AI2 Wins: ${this.ai1Wins}/${this.ai2Wins}`);
			console.log("");
		}

		console.log(`AI1/AI2 Wins: ${this.ai1Wins}/${this.ai2Wins}`);
	}
}

const game = new BattleshipGame();
game.runSimulation(1000);

const textarea = document.querySelector("textarea")
textarea.innerHTML = `${game.board.length}; ${game.board[0].length}`
